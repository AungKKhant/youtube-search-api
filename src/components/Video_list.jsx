import React from 'react';
import VideoDetail from './Video_detail';
import VideoListItem from './Video_list_item';

const VideoList = (props) =>{
    const{videos,onSelectVideo,selectedVideo} = props;

    // const videoItem = 'hello';

    return( 
    <ul className = "list-group">
        {videos.map((video) => {
            
            return (<VideoListItem video = {video} key={video.etag}
                onClick = {() => onSelectVideo(video)}
                selectedVideo = {selectedVideo}
            />
            );
             } )}
    </ul>
    );
};
export default VideoList;