import React from 'react';

class SearchBar extends React.Component{

    state = {
        term: "",
    };

    onInputChange = (event)=>{
        console.log(event.target.value);
        this.setState({
            term:event.target.value,
        });
        this.props.onSearch(event.target.value);
    };
    render(){
        const{term} = this.state;

        return(
        <div className = "my-5">
            <input placeholder = "Search ..." 
            className = "form-control" 
            onChange = {this.onInputChange}
            value={term}
            />
            </div>
            );
    }
            
    }
export default SearchBar;