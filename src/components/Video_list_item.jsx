import React from 'react';

const VideoListItem = props => {
    const { video, onCLick } = props;
    const imgUrl = video.snippet.thumbnails.default.url;
    // const isSelected = video === selectedVideo;
    return (
        <li className="list-group-item d-flex justify-content-between" onClick={onCLick}>
            <div className="video-list media">
                <img alt="" src={imgUrl} className="viedo-item-img" />
            </div>
            <div className="media-body">
                <div className="media-heading ml-5">
                    {video.snippet.title}
                </div>
            </div>

        </li>
    )
}
export default VideoListItem;