import React from 'react';
import _ from 'lodash';
import SearchBar from "./components/Search_bar";
import YTSearch from "youtube-api-search";
import VideoDetail from './components/Video_detail';
import VideoList from './components/Video_list';

 const API_KEY = "AIzaSyA3XPJpRyH5bnOTcoIRqlrwRQ9sXqBu4L0"; 

class App extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      videos: [],
      selectedVideo: null,
    }
   
  } 

  onSearchVideo = (value)=>{
    YTSearch({key: API_KEY , term: value},(data)=>{
      //  console.log(data);
       this.setState({
         videos : data,
         selectedVideo: data[0]
       })
      });
  
  }
  render(){
    const videoSearch = _.debounce(value=>{
      this.onSearchVideo(value);
    }, 3000);
    return(
    <div className="container">
      <SearchBar onSearch = {videoSearch}/>
      <div className = "row">
        <div className = "col-md-8">

      <VideoDetail video = {this.state.selectedVideo} />
      </div>
      <div className = "col-md-4">
        <VideoList onSelectVideo = {(video)=>this.setState({selectedVideo : video})} videos ={this.state.videos}
        selectedVideo={this.state.selectedVideo} />
      </div>
    </div>
    </div> 
    );
  }
}
export default App;
